
<?php
class TestModel extends CI_Model {
	public $idAlum;
	public $numAciertos;
	public $fecha;
	public function crearPregunta($pregunta, $correcta, $res1, $res2, $res3) {
		$id = $this->buscarId () + 1;
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		$preguntaXML = $test->addChild ( 'pregunta' );
		$preguntaXML->addChild ( 'enunciado', $pregunta );
		$preguntaXML->addAttribute ( 'id', $id );
		$res1XML = $preguntaXML->addChild ( 'respuesta', $res1 );
		$res2XML = $preguntaXML->addChild ( 'respuesta', $res2 );
		$res3XML = $preguntaXML->addChild ( 'respuesta', $res3 );
		
		switch ($correcta) {
			case 0 :
				$res1XML->addAttribute ( 'correcta', 'si' );
				$res2XML->addAttribute ( 'correcta', 'no' );
				$res3XML->addAttribute ( 'correcta', 'no' );
				break;
			case 1 :
				$res1XML->addAttribute ( 'correcta', 'no' );
				$res2XML->addAttribute ( 'correcta', 'si' );
				$res3XML->addAttribute ( 'correcta', 'no' );
				break;
			case 2 :
				$res1XML->addAttribute ( 'correcta', 'no' );
				$res2XML->addAttribute ( 'correcta', 'no' );
				$res3XML->addAttribute ( 'correcta', 'si' );
				break;
		}
		$test->asXML ( 'assets/XML/test.xml' );
	}
	public function buscarId() {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		$ultima = $test->pregunta [$test->count () - 1];
		if ($ultima ['id'] > 0) {
			return $ultima ['id'];
		} else {
			return 0;
		}
	}
	public function getPreguntas() {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		return $test;
	}
	public function getPregunta($num) {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		
		// hay que hacer un cast
		return $test->pregunta [( int ) $num];
	}
	public function borrarPregunta($num) {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		/*
		 * foreach($doc->seg as $seg)
		 * {
		 * if($seg['id'] == 'A12') {
		 * $dom=dom_import_simplexml($seg);
		 * $dom->parentNode->removeChild($dom);
		 * }
		 * }
		 */
		$pre = $test->pregunta [( int ) $num];
		$dom = dom_import_simplexml ( $pre );
		$dom->parentNode->removeChild ( $dom );
		$test->asXML ( 'assets/XML/test.xml' );
	}
	public function modificarPregunta($pregunta, $correcta, $res1, $res2, $res3, $id) {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		foreach ( $test->pregunta as $preg ) {
			if ($preg ['id'] == $id) {
				$dom = dom_import_simplexml ( $preg );
				$dom->parentNode->removeChild ( $dom );
				$test->asXML ( 'assets/XML/test.xml' );
			}
		}
		
		$this->crearPregunta ( $pregunta, $correcta, $res1, $res2, $res3 );
	}
	public function testAlumno() {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		$numeros = $this->numAleatorios ();
		$preguntas = array ();
		for($i = 0; $i < 20; $i ++) {
			// $preguntas[]=$test->pregunta[$numeros[$i]];
			$preguntas [$i] ['enunciado'] = $test->pregunta [$numeros [$i]]->enunciado;
			$preguntas [$i] ['res1'] = $test->pregunta [$numeros [$i]]->respuesta [0];
			$preguntas [$i] ['res2'] = $test->pregunta [$numeros [$i]]->respuesta [1];
			$preguntas [$i] ['res3'] = $test->pregunta [$numeros [$i]]->respuesta [2];
			if ($test->pregunta [$numeros [$i]]->respuesta [0] ['correcta'] == 'si') {
				$preguntas [$i] ['correcta'] = 0;
			} else if ($test->pregunta [$numeros [$i]]->respuesta [1] ['correcta'] == 'si') {
				$preguntas [$i] ['correcta'] = 1;
			} else {
				$preguntas [$i] ['correcta'] = 2;
			}
			$preguntas [$i] ['id'] = $test->pregunta [$numeros [$i]] ['id'];
		}
		return $preguntas;
	}
	public function numAleatorios() {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		$max = $test->count () - 1;
		$arrayNumeros = range ( 0, $max );
		shuffle ( $arrayNumeros );
		return $arrayNumeros;
	}
	public function corregirTest($post) {
		$cont = 0;
		$arrayRes = unserialize ( $post ['Final'] );
		for($i = 0; $i < 20; $i ++) {
			$auxPre = "Pregunta" . $i;
			
			if ($post [$auxPre] == $arrayRes [$i]) {
				$cont ++;
			}
		}
		// Insertar en base de datos
		$id = $post ['id'];
		$this->load->database ();
		$this->idAlum = $post ['id'];
		$this->numAciertos = $cont;
		$this->fecha = date ( "d/m/Y" ) . ' ' . date ( "h:m" );
		$this->db->insert ( 'resultados', $this );
	}
	public function testInvitado() {
		$test = new SimpleXMLElement ( 'assets/XML/test.xml', null, true );
		$numeros = $this->numAleatorios ();
		$preguntas = array ();
		for($i = 0; $i < 10; $i ++) {
			// $preguntas[]=$test->pregunta[$numeros[$i]];
			$preguntas [$i] ['enunciado'] = $test->pregunta [$numeros [$i]]->enunciado;
			$preguntas [$i] ['res1'] = $test->pregunta [$numeros [$i]]->respuesta [0];
			$preguntas [$i] ['res2'] = $test->pregunta [$numeros [$i]]->respuesta [1];
			$preguntas [$i] ['res3'] = $test->pregunta [$numeros [$i]]->respuesta [2];
			if ($test->pregunta [$numeros [$i]]->respuesta [0] ['correcta'] == 'si') {
				$preguntas [$i] ['correcta'] = 0;
			} else if ($test->pregunta [$numeros [$i]]->respuesta [1] ['correcta'] == 'si') {
				$preguntas [$i] ['correcta'] = 1;
			} else {
				$preguntas [$i] ['correcta'] = 2;
			}
			$preguntas [$i] ['id'] = $test->pregunta [$numeros [$i]] ['id'];
		}
		return $preguntas;
	}
	public function listarResultados($id) {
		
		// Preparar la lista de resultados
		$this->load->database ();
		$datos = $this->db->query ( "
				SELECT fecha, numAciertos
				FROM resultados
				WHERE idAlum = '$id'" )->result ();
		return $datos;
	}
}

?>
