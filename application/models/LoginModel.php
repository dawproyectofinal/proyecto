
<?php
require_once APPPATH . 'models/UsuarioModel.php';
class LoginModel extends CI_Model {
	public $id;
	public $nombre;
	public $apellidos;
	public $fnac;
	public $email;
	public $usuario;
	public $password;
	public $perfil; // 0 -> ALUMNO | 1 -> PROFESOR 2-> ADMINISTRADOR
	public function validarUsuario($usuario, $clave) {
		$this->load->database ();
		
		$usuarioModel_instance = new UsuarioModel ();
		
		$validacion = false;
		// selecciono el usuario y la password donde el usuario sea igual al introducido
		$query = $this->db->select ( 'usuario, password' );
		$query = $this->db->where ( 'usuario', $usuario );
		$query = $this->db->get ( 'usuarios' );
		
		// si devuelve fila saco la password para desencriptar
		
		if ($query->row ()) {
			foreach ( $query->result () as $datos ) {
				
				$password = $datos->password;
			}
			// desencripto la password para compararla con la ingresada por el usuario
			$password = $this->getPasswordDesencriptada ( $usuario );
			
			// echo "password despues de desencriptar: ".$password;
			
			if (( string ) $clave == ( string ) $password) {
				
				$validacion = true;
			} else
				$validacion = false;
		}
		
		// echo var_dump($validacion);
		
		return $validacion;
	}
	
	// devuelve la contraseña desencriptada
	public function getPasswordDesencriptada($usuario) {
		$this->load->database ();
		$usuarioModel_instance = new UsuarioModel ();
		
		// select de password
		$query = $this->db->select ( "password" );
		$query = $this->db->where ( "usuario", $usuario );
		$query = $this->db->get ( "usuarios" );
		
		foreach ( $query->result () as $datoPassword ) {
			$password = $datoPassword->password;
		}
		
		// desencripto la password
		$passwordDesencriptada = $usuarioModel_instance->decrypt ( $password );
		// echo "password desencriptada: ".$passwordDesencriptada;
		
		return $passwordDesencriptada;
	}
	
	// devuelve el perfil
	public function getPerfil($usuario, $clave) {
		$this->load->database ();
		
		$query = $this->db->select ( "perfil" );
		$query = $this->db->where ( "usuario", $usuario );
		$query = $this->db->get ( "usuarios" );
		
		foreach ( $query->result () as $datoPerfil ) {
			$perfil = $datoPerfil->perfil;
		}
		
		return $perfil;
	}
	
	// devuelve el id
	public function getId($usuario, $clave) {
		$this->load->database ();
		
		$query = $this->db->select ( "id" );
		$query = $this->db->where ( "usuario", $usuario );
		$query = $this->db->get ( "usuarios" );
		
		foreach ( $query->result () as $datoId ) {
			$id = $datoId->id;
		}
		
		return $id;
	}
	
	// devuelve el id
	public function getNombre($usuario, $clave) {
		$this->load->database ();
		
		$query = $this->db->select ( "nombre" );
		$query = $this->db->where ( "usuario", $usuario );
		$query = $this->db->get ( "usuarios" );
		
		foreach ( $query->result () as $datoNombre ) {
			$nombre = $datoNombre->nombre;
		}
		
		return $nombre;
	}
}

?>