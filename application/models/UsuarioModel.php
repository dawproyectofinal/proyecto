
<?php
class UsuarioModel extends CI_Model {
	public $id;
	public $nombre;
	public $apellidos;
	public $fnac;
	public $email;
	public $usuario;
	public $password;
	public $perfil; // 0 -> ALUMNO | 1 -> PROFESOR
	                
	// ====================================ENCRYPTION PASSWORDS
	private static $Key = "dublin";
	public static function encrypt($input) {
		$output = base64_encode ( mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, md5 ( UsuarioModel::$Key ), $input, MCRYPT_MODE_CBC, md5 ( md5 ( UsuarioModel::$Key ) ) ) );
		return $output;
	}
	public static function decrypt($input) {
		$output = rtrim ( mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, md5 ( UsuarioModel::$Key ), base64_decode ( $input ), MCRYPT_MODE_CBC, md5 ( md5 ( UsuarioModel::$Key ) ) ), "\0" );
		return $output;
	}
	
	// ====================================Devuelve todos los usuarios registrados que haya.
	public function getTodosUsuarios() {
		$this->load->database ();
		$datos = $this->db->get ( 'usuarios' )->result ();
		return $datos;
	}
	
	// ====================================Inserta un nuevo usuario en la tabla 'usuarios'.
	public function insertarUsuario($nombre, $apellidos, $fnac, $email, $usuario, $password, $perfil) {
		$this->load->database ();
		$ultimo_id = $this->db->insert_id ();
		
		// Metodo 1 CRYPT
		// $password = crypt($password, '$5$rounds=5000$usesomesillystringforsalt$');//CRYPT_SHA256
		
		// Metodo 2 base64_encode_decode
		$password = UsuarioModel::encrypt ( $password );
		// Desencriptamos el texto
		// $texto_original = UsuarioModel::decrypt($texto_encriptado);
		
		$this->id = $ultimo_id;
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->fnac = $fnac;
		$this->email = $email;
		$this->usuario = $usuario;
		$this->password = $password;
		$this->perfil = $perfil;
		$this->db->insert ( 'usuarios', $this );
	}
	
	// ====================================Modifica los datos de un usuario.
	public function modificarUsuario($id, $nombre, $apellidos, $fnac, $email, $user, $password, $perfil) {
		$this->load->database ();
		$this->db->query ( "UPDATE usuarios SET nombre = '$nombre' WHERE id = '$id';" );
		$this->db->query ( "UPDATE usuarios SET apellidos = '$apellidos' WHERE id = '$id';" );
		$this->db->query ( "UPDATE usuarios SET fnac = '$fnac' WHERE id = '$id';" );
		$this->db->query ( "UPDATE usuarios SET email = '$email' WHERE id = '$id';" );
		$this->db->query ( "UPDATE usuarios SET usuario = '$user' WHERE id = '$id';" );
		if ($password != '') { // por si no quiere modificar la contraseña.
			$password = UsuarioModel::encrypt ( $password );
			$this->db->query ( "UPDATE usuarios SET password = '$password' WHERE id = '$id';" );
		}
		
		$this->db->query ( "UPDATE usuarios SET perfil = '$perfil' WHERE id = '$id';" );
	}
	
	// ====================================Borra un usuario.
	public function borrarUsuario($id) {
		$this->load->database ();
		$this->db->query ( "DELETE FROM usuarios  WHERE id = '$id';" );
	}
	
	// ====================================Devuelve los datos de un usuario desde su ID.
	public function getDatosUsuario($id) {
		$this->load->database ();
		$datos = $this->db->query ( "
				SELECT ID, NOMBRE, APELLIDOS, FNAC, EMAIL, USUARIO, PASSWORD, PERFIL
				FROM USUARIOS
				WHERE ID = '$id'" )->result ();
		return $datos;
	}
	
	// ====================================Devuelve los datos de un usuario filtrado por el perfil.
	public function getDatosPerfil($perfil) {
		$this->load->database ();
		$datos = $this->db->query ( "
				SELECT ID, NOMBRE, APELLIDOS, FNAC, EMAIL, USUARIO, PASSWORD, PERFIL
				FROM USUARIOS
				WHERE PERFIL = '$perfil'" )->result ();
		return $datos;
	}
}

?>
