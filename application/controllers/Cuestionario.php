
<?php
//CONTROLADOR USUARIO
if(!isset($_SESSION)) { session_start(); }
class Cuestionario extends CI_Controller{	
	//Muestra la página de alta de usuarios.
	public function index(){
		$this->load->view('templates/head');
		$this->load->view('templates/header');
		$this->load->view('Test/CrearTestView');
		$this->load->view('templates/footer');
	}
	public function crearPregunta(){
		$this->load->model('TestModel');

		

		if ($_POST['pregunta']!=''&&$_POST['correcta']!=''&&$_POST['res1']!=''&&$_POST['res2']!=''&&$_POST['res3']!=''){
			$this->TestModel->crearPregunta($_POST['pregunta'],$_POST['correcta'],$_POST['res1'],$_POST['res2'],$_POST['res3']);

			$datos['info']="Todo correcto";
		}else{
			$datos['info']="Debes rellenar todos los campos";
		}
		
		$this->load->view('templates/head');
		$this->load->view('templates/header');
		$this->load->view('Test/CrearTestView',$datos);
		$this->load->view('templates/footer');
	}
	public function listarPreguntas(){
		$this->load->model('TestModel');
		$datos['preguntas'] = $this->TestModel->getPreguntas();
		$this->load->view('templates/head');
		$this->load->view('templates/header');
		$this->load->view('Test/listar',$datos);
		$this->load->view('templates/footer');
	}
	public function modificarPregunta(){
		if (isset($_GET['num'])){
			$this->load->model('TestModel');
			$datos['pregunta'] = $this->TestModel->getPregunta($_GET['num']);
			$this->load->view('templates/head');
			$this->load->view('templates/header');
			$this->load->view('Test/modificar',$datos);
			$this->load->view('templates/footer');
		}
		
	}
	public function modificarPost(){
		$this->load->model('TestModel');
		$this->TestModel->modificarPregunta($_POST['pregunta'],$_POST['correcta'],$_POST['res1'],$_POST['res2'],$_POST['res3'],$_POST['id']);
		$this->listarPreguntas();
	
	}
	public function borrarPregunta(){
		if (isset($_GET['num'])){
			$this->load->model('TestModel');
			$this->TestModel->borrarPregunta($_GET['num']);
			header('Location: listarPreguntas');
		}
	
	}
}


?>