
<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
// CONTROLADOR USUARIO ADMINISTRADOR
class Usuario extends CI_Controller {
	
	// ======================================== Pagina principal PORTADA.
	public function index() {
		$this->load->view ( 'templates/head' );
		
		$this->load->view ( 'templates/header' );
		
		$this->load->view ( 'Usuario/PortadaView' );
		$this->load->view ( 'templates/footer' );
	}
	
	// ======================================== Muestra la pagina de REGISTRO de usuarios.
	public function registro() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		
		$this->load->model ( 'UsuarioModel' );
		$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
		
		$cadena = '';
		foreach ( $datos ['usuarios'] as $user ) {
			$cadena = $cadena . $user->usuario;
		}
		$datos ['usuarios'] = $cadena;
		$this->load->view ( 'Usuario/RegistroView', $datos );
		$this->load->view ( 'templates/footer' );
	}
	
	// ======================================== CREAR nuevo usuario.
	public function crear() {
		$nombres = $_POST ['nombre'];
		$apellidos = $_POST ['apellidos'];
		// var_dump($nombres);
		
		echo $_POST ['user'];
		echo $_POST ['password'];
		echo $_POST ['perfil'];
		
		// ==================== Convertir letras UPPER CASE
		function ucnombres($string) {
			$string = ucwords ( strtolower ( $string ) );
			
			foreach ( array (
					'-',
					'\'' 
			) as $delimiter ) {
				if (strpos ( $string, $delimiter ) !== false) {
					$string = implode ( $delimiter, array_map ( 'ucfirst', explode ( $delimiter, $string ) ) );
				}
			}
			return $string;
		}
		// ================================================
		
		$nombres = ucnombres ( $nombres );
		$apellidos = ucnombres ( $apellidos );
		
		// Formatamos la fecha
		$porciones = explode ( "/", $_POST ['fnac'] );
		$_POST ['fnac'] = $porciones [2] . '-' . $porciones [1] . '-' . $porciones [0];
		// ===================
		
		$this->load->model ( 'UsuarioModel' );
		$this->UsuarioModel->insertarUsuario ( $nombres, $apellidos, $_POST ['fnac'], $_POST ['email'], $_POST ['user'], $_POST ['password'], $_POST ['perfil'] );
		$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
		
		// header("Location: listar");
		
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Usuario/UsuariosView', $datos );
		$this->load->view ( 'templates/footer' );
	}
	
	// ======================================== LISTA TODOS los usuarios.
	public function listar() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->model ( 'UsuarioModel' );
		$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
		
		// Formatamos la fecha
		foreach ( $datos ['usuarios'] as $c => $v ) {
			if ($v->fnac) {
				$porciones = explode ( "-", $v->fnac );
				$v->fnac = $porciones [2] . '/' . $porciones [1] . '/' . $porciones [0];
			}
		}
		
		$this->load->view ( 'Usuario/UsuariosView', $datos );
		$this->load->view ( 'templates/footer' );
	}
	
	// ======================================== FILTRA el listado por perfil.
	public function filtrar() {
		$this->load->model ( 'UsuarioModel' );
		
		$datos ['usuarios'] = $this->UsuarioModel->getDatosPerfil ( $_GET ['perfil'] );
		// $datos['usuarios'] = array_change_key_case($datos['usuarios'], CASE_LOWER);
		// var_dump($datos['usuarios']);
		
		// Formatamos la fecha
		foreach ( $datos ['usuarios'] as $c => $v ) {
			if ($c == 'FNAC') {
				$porciones = explode ( "-", $v->fnac );
				$v->fnac = $porciones [2] . '/' . $porciones [1] . '/' . $porciones [0];
			}
		}
		$this->load->view ( 'Usuario/UsuariosView', $datos );
	}
	
	// ======================================== Identifica el tipo de Accion MODIFICAR / BORRAR.
	public function acciones() {
		$this->load->model ( 'UsuarioModel' );
		
		if ($_GET ['accion'] == 'M') { // Modificar
			$datos ['usuario'] = $this->UsuarioModel->getDatosUsuario ( $_GET ['idusuario'] );
			
			// =================== Formata la fecha.
			foreach ( $datos ['usuario'] as $c => $v ) {
				if ($c == 'FNAC') {
					$porciones = explode ( "-", $v->fnac );
					$v->fnac = $porciones [2] . '/' . $porciones [1] . '/' . $porciones [0];
				}
				// ///////////////////////////////////
				if ($c == 'USUARIO') { // aqui cojemos en usuario para eliminarlo de la cadena mas abajo.
					$usuario = $v->usuario;
				}
			}
			// ===================
			// //////////////////////////////////
			// =================== Coge los datos de los usuarios y encadena en String el campo USUARIO de cada uno. (usado para comprobar si ya existe el usuario/login).
			$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
			// var_dump($datos['usuarios']);
			$cadena = '';
			foreach ( $datos ['usuarios'] as $user ) {
				$cadena = $cadena . $user->usuario;
			}
			
			$cadena = str_replace ( $usuario, "", $cadena ); // aqui eliminamos el usuario de la cadena, porque si no sale como usuario existente y no deja modificar otros campos.
			
			$datos ['usuarios'] = $cadena;
			// ===================
			
			$this->load->view ( 'Usuario/ModificarView', $datos );
		} else if ($_GET ['accion'] == 'B') { // Borrar
			$this->UsuarioModel->borrarUsuario ( $_GET ['idusuario'] );
			$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
			$this->load->view ( 'Usuario/UsuariosView', $datos );
		}
	}
	
	// ======================================== Funccion MODIFICAR datos de un usuario existente.
	public function modificar() {
		$this->load->model ( 'UsuarioModel' );
		
		$porciones = explode ( "/", $_POST ['fnac'] );
		$_POST ['fnac'] = $porciones [2] . '-' . $porciones [1] . '-' . $porciones [0];
		
		$this->UsuarioModel->modificarUsuario ( $_POST ['id'], $_POST ['nombre'], $_POST ['apellidos'], $_POST ['fnac'], $_POST ['email'], $_POST ['user'], $_POST ['password'], $_POST ['perfil'] );
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$datos ['usuarios'] = $this->UsuarioModel->getTodosUsuarios ();
		
		// Formatamos la fecha
		foreach ( $datos ['usuarios'] as $c => $v ) {
			if ($v->fnac) {
				$porciones = explode ( "-", $v->fnac );
				$v->fnac = $porciones [2] . '/' . $porciones [1] . '/' . $porciones [0];
			}
		}
		// ===================
		$this->load->view ( 'Usuario/UsuariosView', $datos );
		$this->load->view ( 'templates/footer' );
		// header("Location: listar");
	}
}

?>
