
<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
class Alumno extends CI_Controller {
	public function index() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/portadaAlumno' );
		$this->load->view ( 'templates/footer' );
	}
	public function realizarTest() {
		$this->load->model ( 'TestModel' );
		$datos ['preguntas'] = $this->TestModel->testAlumno ();
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/Test', $datos );
		$this->load->view ( 'templates/footer' );
	}
	public function corregirTest() {
		$this->load->model ( 'TestModel' );
		$datos ['resultados'] = $this->TestModel->corregirTest ( $_POST );
		/*
		 * $this->load->view('templates/head');
		 * $this->load->view('templates/header');
		 * $this->load->view('Alumno/resultados',$datos);
		 * $this->load->view('templates/footer');
		 */
		header ( 'Location: listarResultados' );
	}
	public function listarResultados() {
		$this->load->model ( 'TestModel' );
		$datos ['resultados'] = $this->TestModel->listarresultados ( $_SESSION ['id'] );
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/resultados', $datos );
		$this->load->view ( 'templates/footer' );
	}
	public function quienesSomos() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/quienesSomos' );
		$this->load->view ( 'templates/footer' );
	}
	public function contacto() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/contacto' );
		$this->load->view ( 'templates/footer' );
	}
	public function verOfertas() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/verOfertas' );
		$this->load->view ( 'templates/footer' );
	}
}

?>