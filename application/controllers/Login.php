<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
class Login extends CI_Controller {
	public function index() {
		
		// llamamos desde el login al inicio de la aplicación que será la vista de invitado
		if (! isset ( $_SESSION )) {
			session_start ();
		}
		$_SESSION ['invitado'] = "invitado";
		
		if (isset ( $_SESSION ['invitado'] )) {
			$this->load->view ( 'templates/head' );
			$this->load->view ( 'templates/header' );
			$this->load->view ( 'Invitado/portadaInvitado' );
			$this->load->view ( 'templates/footer' );
		}
	}
	public function cerrar() {
		// if(!isset($_SESSION)) { session_start(); }
		session_destroy ();
		// $this->index ();
		header ( 'Location: index' );
	}
	public function control() {
		$this->load->model ( 'LoginModel' );
		
		$ExisteUsuarioyPassword = $this->LoginModel->validarUsuario ( $_POST ['usuario'], $_POST ['clave'] );
		// comprobamos que el usuario exista en la base de datos y la password ingresada sea correcta
		
		if ($ExisteUsuarioyPassword) {
			// La variable $ExisteUsuarioyPassoword recibe valor TRUE si el usuario existe
			// y FALSE en caso que no.
			// Este valor lo determina el modelo.
			
			// saco el perfil de ese usuario para su posterior vista
			$perfil = $this->LoginModel->getPerfil ( $_POST ['usuario'], $_POST ['clave'] );
			
			// saco el id del usuario logeado para identificarlo a la hora de realizar test
			// y del histórico de resultados
			
			$id = $this->LoginModel->getId ( $_POST ['usuario'], $_POST ['clave'] );
			
			// saco el nombre del usuario introducido
			$nombre = $this->LoginModel->getNombre ( $_POST ['usuario'], $_POST ['clave'] );
			
			// nivel ADMINISTRADOR
			if ($perfil == "2") {
				if (! isset ( $_SESSION )) {
					session_start ();
				}
				// session_start ();
				$_SESSION ['usuario'] = "usuario";
				$_SESSION ['nombre'] = ( string ) $nombre;
				
				/*
				 * $this->load->view ( 'templates/head' );
				 * $this->load->view ( 'templates/header' );
				 * $this->load->view ( 'Usuario/PortadaView' );
				 * $this->load->view ( 'templates/footer' );
				 */
				redirect ( 'Usuario' );
			}
			// nivel PROFESOR
			if ($perfil == "1") {
				if (! isset ( $_SESSION )) {
					session_start ();
				}
				// session_start ();
				$_SESSION ['profesor'] = "profesor";
				$_SESSION ['nombre'] = ( string ) $nombre;
				
				/*
				 * $this->load->view ( 'templates/head' );
				 * $this->load->view ( 'templates/header' );
				 * $this->load->view ( 'Profesor/portadaProfesor' );
				 * $this->load->view ( 'templates/footer' );
				 */
				redirect ( 'Profesor' );
			}
			// nivel ALUMNO
			if ($perfil == "0") {
				if (! isset ( $_SESSION )) {
					session_start ();
				}
				// session_start ();
				$_SESSION ['alumno'] = "alumno";
				$_SESSION ['nombre'] = ( string ) $nombre;
				$_SESSION ['id'] = $id;
				
				/*
				 * $this->load->view ( 'templates/head' );
				 * $this->load->view ( 'templates/header' );
				 * $this->load->view ( 'Alumno/portadaAlumno' );
				 * $this->load->view ( 'templates/footer' );
				 */
				// header ( 'Location: Alumno/index' );
				redirect ( 'Alumno' );
			}
		} else { // Si no logró validar
			
			$this->index ();
			$this->load->view ( 'errors/error_login' );
		}
	}
}

?>