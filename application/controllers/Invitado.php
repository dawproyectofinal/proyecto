<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
class Invitado extends CI_Controller {
	public function index() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Invitado/portadaInvitado' );
		$this->load->view ( 'templates/footer' );
	}
	public function realizarTest() {
		$this->load->model ( 'TestModel' );
		$datos ['preguntas'] = $this->TestModel->testInvitado ();
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Invitado/Test', $datos );
		$this->load->view ( 'templates/footer' );
	}
	public function corregirTest() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Alumno/verOfertas' );
		$this->load->view ( 'templates/footer' );
	}
	public function quienesSomos() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Invitado/quienesSomos' );
		$this->load->view ( 'templates/footer' );
	}
	public function contacto() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Invitado/contacto' );
		$this->load->view ( 'templates/footer' );
	}
	public function verOfertas() {
		$this->load->view ( 'templates/head' );
		$this->load->view ( 'templates/header' );
		$this->load->view ( 'Invitado/verOfertas' );
		$this->load->view ( 'templates/footer' );
	}
}
?>
