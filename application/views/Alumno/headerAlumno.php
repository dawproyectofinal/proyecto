
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/realizarTest"><span
				class="glyphicon glyphicon-check"></span> Test</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>assets/calendario/indexAlumno.php"><span
				class="glyphicon glyphicon-calendar"></span> Calendario</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/verOfertas"> <span
				class="glyphicon glyphicon-eur"></span> Ofertas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/quienesSomos"> <span
				class="glyphicon glyphicon-info-sign"></span> Quiénes somos
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/contacto"> <span
				class="glyphicon glyphicon-phone-alt"></span> Contacto
			</a>
		</div>
		<div class="navbar-header" id="estado">
				<?= isset($_SESSION['alumno'])?'<b>Hola '.$_SESSION['alumno'].'</b>':'<b>Estas como INVITADO</b>'?>
			</div>

		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>
	</nav>
	<!--/.navbar-collapse -->


</header>
<div id="contenedorPrincipal">

