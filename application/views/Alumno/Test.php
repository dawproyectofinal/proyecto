
<div id="test">

<?php
$numeracion = 1;
$aux = 0;
$arrayAux = array ();
foreach ( $preguntas as $pregunta ) {
	$arrayAux [] = ( int ) $pregunta ['correcta'];
}
?>
<form class="form-horizontal"
		action="<?= base_url()?>Alumno/corregirTest" method="Post">
<?php foreach($preguntas as $pregunta): ?>
		
	<div id="contenedorPregunta">
			<!-- enunciado -->
			<div class="form-group">
				<div id="numeracion"><?= $numeracion.'. '?></div>
				<label class="col-md-4 control-label" id="enunciado"><h2><?=$pregunta['enunciado']?></h2></label><br>
			</div>

			<!-- respuestas -->
			<div class="form-group">
				<div class="col-md-4">
					<div class="radio" id="respuesta">
						<label id="respuesta"> <span></span><input type="radio"
							name="Pregunta<?= $aux?>" value="3" checked id="radioSprite"> Sin
							contestar<br>
						</label>
					</div>

					<div class="radio" id="respuesta">
						<label id="respuesta"> <span></span><input type="radio"
							name="Pregunta<?= $aux?>" value="0"><?= $pregunta['res1']?><br>
						</label>
					</div>

					<div class="radio" id="respuesta">
						<label id="respuesta"> <span></span><input type="radio"
							name="Pregunta<?= $aux?>" value="1"><?= $pregunta['res2']?><br>
						</label>
					</div>

					<div class="radio" id="respuesta">
						<label id="respuesta"> <span></span><input type="radio"
							name="Pregunta<?= $aux?>" value="2"><?= $pregunta['res3']?>
			</label>
					</div>

				</div>
			</div>
		</div>
		<div id="separador"></div>	
	<?php $aux++; $numeracion++;?>		
<?php endforeach; ?>
	<input type="hidden" name="Final" value="<?= serialize($arrayAux)?>"> <input
			type="hidden" name="id" value="<?= $_SESSION['id']?>">

		<!-- Enviar/Cancelar -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="alta"></label>
			<div class="col-md-8" id="botonesEnviarCancelar">
				<button type="submit" id="alta" name="alta" class="btn btn-success"
					value="Enviar">
					<i class="fa fa-check-square-o fa-2x"> Finalizar<br>Test
					</i>
				</button>

			</div>
		</div>

		<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
		<br> <br>
	</form>

</div>

