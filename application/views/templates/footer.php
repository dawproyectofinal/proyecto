</div>
<!-- FOOTER -->
<footer>
	<div id="footer1">
		<div id="footerRedesSociales">
			<a href="http://www.facebook.com" class="btn btn-link" role="button">
				<i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i>
			</a> <a href="http://www.skype.com" class="btn btn-link"
				role="button"> <i class="fa fa-skype fa-2x" aria-hidden="true"></i>
			</a> <a href="http://www.twitter.com" class="btn btn-link"
				role="button"> <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
			</a> <a href="http://www.whatsapp.com" class="btn btn-link"
				role="button"> <i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
			</a> <a href="http://www.youtube.com" class="btn btn-link"
				role="button"> <i class="fa fa-youtube fa-2x" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	<div id="footer2">COPYRIGHT © 2016 Autoescuela Olmos. Todos los
		derechos reservados</div>
</footer>

<!-- /.container -->
<!-- Librería jQuery requerida por los plugins de JavaScript -->
<script src="http://code.jquery.com/jquery.js"></script>

<!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
</body>
</html>