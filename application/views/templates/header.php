
<?php if (isset($_SESSION['invitado'])) :?>
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/realizarTest"><span
				class="glyphicon glyphicon-check"></span> Test</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/verOfertas"> <span
				class="glyphicon glyphicon-eur"></span> Ofertas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/quienesSomos">
				<span class="glyphicon glyphicon-info-sign"></span> Quiénes somos
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/contacto"> <span
				class="glyphicon glyphicon-phone-alt"></span> Contacto
			</a>
		</div>

		<div class="navbar-header" id="estado">
				<?= isset($_SESSION['invitado'])?'<b>Hola '.$_SESSION['invitado'].'</b>':'<b>Estas como INVITADO</b>'?>
			</div>

		<div id="navbar" class="navbar-collapse collapse">
			<form class="navbar-form navbar-right" id="formInicioSesion"
				method="post" action="<?= base_url()?>Login/control">
				<div class="form-group">

					<input type="text" name="usuario" size="10" placeholder="Usuario"
						class="form-control">
				</div>
				<div class="form-group">
					<input type="password" name="clave" size="10"
						placeholder="Contraseña" class="form-control">
				</div>
				<button type="submit" id="btnIniciarSesion" class="btn btn-success">Iniciar
					sesión</button>
			</form>
		</div>
	</nav>
	<!--/.navbar-collapse -->


</header>

<?php endif;?>

<?php if (isset($_SESSION['alumno'])) :?>
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/realizarTest"><span
				class="glyphicon glyphicon-check"></span> Test</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>assets/calendario/indexAlumno.php"><span
				class="glyphicon glyphicon-calendar"></span> Calendario</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/verOfertas"> <span
				class="glyphicon glyphicon-eur"></span> Ofertas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/quienesSomos"> <span
				class="glyphicon glyphicon-info-sign"></span> Quiénes somos
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/contacto"> <span
				class="glyphicon glyphicon-phone-alt"></span> Contacto
			</a>
		</div>
		<div class="navbar-header" id="estado">
				<?= isset($_SESSION['alumno'])?'<b>Hola '.$_SESSION['nombre'].' ('.$_SESSION['alumno'].')</b>':'<b>Estas como INVITADO</b>'?>
			</div>

		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>
	</nav>
	<!--/.navbar-collapse -->
</header>


<?php endif;?>
	
<?php if (isset($_SESSION['profesor'])) :?>

<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Profesor/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>assets/calendario/index.php"><span
				class="glyphicon glyphicon-calendar"></span> Ver/editar Calendario</a>
		</div>

		<div class="navbar-header" id="estado">
				<?= isset($_SESSION['profesor'])?'<b>Hola '.$_SESSION['nombre'].' ('.$_SESSION['profesor'].')</b>':'<b>Estas como INVITADO</b>'?>
			</div>

		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>
	</nav>
	<!--/.navbar-collapse -->


</header>


<?php endif;?>

<?php
if (isset ( $_SESSION ['usuario'] )) :
	?>

<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Usuario/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Usuario/registro"><span
				class="glyphicon glyphicon-user"></span> Crear Usuarios</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Usuario/listar"> <span
				class="glyphicon glyphicon-th-list"></span> Listar Usuarios
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Cuestionario/index"> <span
				class="glyphicon glyphicon-tag"></span> Crear Preguntas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>Cuestionario/listarPreguntas"> <span
				class="glyphicon glyphicon-tasks"></span> Listar Preguntas
			</a>
		</div>

		<div class="navbar-header" id="estado">
				<?= isset($_SESSION['usuario'])?'<b>Hola '.$_SESSION['nombre'].' ('.$_SESSION['usuario'].')</b>':'<b>Estas como INVITADO</b>'?>
			</div>

		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>

	</nav>
	<!--/.navbar-collapse -->
</header>


<?php endif;?>


