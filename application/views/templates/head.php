
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Autoescuela Olmos</title>

<!-- CSS de Bootstrap y personalizaciones -->
<link href="<?= base_url()?>assets/css/bootstrap.min.css"
	rel="stylesheet" media="screen">
<link href="<?= base_url()?>assets/css/bootstrapSobreescrito.css"
	rel="stylesheet">

<!-- Librería de jQuery -->
<link rel="stylesheet"
	href="<?= base_url() ?>assets/css/font-awesome.min.css">
<script src="<?= base_url() ?>assets/js/jquery-2.2.3.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.alerts.js"></script>

<!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

