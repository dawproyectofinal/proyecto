

<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/index"><span
				class="glyphicon glyphicon-home"></span> Inicio</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Invitado/realizarTest"><span
				class="glyphicon glyphicon-check"></span> Test</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/verOfertas"> <span
				class="glyphicon glyphicon-eur"></span> Ofertas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/quienesSomos"> <span
				class="glyphicon glyphicon-info-sign"></span> Quiénes somos
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Alumno/contacto"> <span
				class="glyphicon glyphicon-phone-alt"></span> Contacto
			</a>
		</div>

		<div id="usuario"><?= isset($_SESSION['invitado'])?'Hola '.$_SESSION['invitado']:'Estas como INVITADO' ?></div>

		<div id="navbar" class="navbar-collapse collapse">
			<form class="navbar-form navbar-right" id="formInicioSesion"
				method="post" action="<?= base_url()?>Login/control">
				<div class="form-group">

					<input type="text" name="usuario" size="10" placeholder="Usuario"
						class="form-control">
				</div>
				<div class="form-group">
					<input type="password" name="clave" size="10"
						placeholder="Contraseña" class="form-control">
				</div>
				<button type="submit" id="btnIniciarSesion" class="btn btn-success">Iniciar
					sesión</button>
			</form>
		</div>
	</nav>
	<!--/.navbar-collapse -->


</header>
<div id="contenedorPrincipal">