

<div class="textoQuienesSomos">
	<h2>QUIÉNES SOMOS</h2>
	<hr>
	<p>La Autoescuela OLMOS lleva dando servicio en San Fernando de Henares
		desde el año 2010. En estas autoescuelas se han formado a miles de
		conductores a lo largo de todos estos años y seguiremos haciéndolo,
		porque, aunque muchas cosas a lo largo de estos años han cambiado, hay
		algo que permanece, y es la profesionalidad y la seriedad que nos ha
		caracterizado desde nuestros comienzos. Nuestra empresa Autoescuela
		OLMOS, es una de las empresas líderes de su sector, no sólo por el
		número y calidad de medios de que dispone, sino por ser referente
		continúo del resto de las Autoescuelas de San Fernado de Henares, en
		campos como métodos de enseñanza, innovaciones pedagógicas, técnicas
		de gestión y publicidad, búsquedas de nuevos mercados, etc. Sabemos
		que la problemática actual que tiene la sociedad es, en muchos casos,
		falta de tiempo. Por lo que nos adaptamos a sus necesidades, Cursos a
		medida incluso con desplazamiento de nuestro personal a sus
		instalaciones, o si lo prefieren en nuestras propias oficinas. Somos
		una empresa sólida, con una gran experiencia en la formación y un gran
		equipo de profesionales que disponen de todos los medios para que la
		enseñanza sea rápida, efectiva y con buenos resultados. Somos una
		Autoescuela líder en el sector. Consigue con nosotros todos los
		permisos y no te quedes en casa. Tu Autoescuela OLMOS en San Fernando
		de Henares.</p>
</div>


<div class="mapaQuienesSomos">
	<h2>DÓNDE ESTAMOS</h2>
	<hr>
	<iframe
		src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.3461050430387!2d-3.531095384465812!3d40.42333446316145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42309caa01764d%3A0xcaf177acf3c669ef!2sInstituto+de+Educaci%C3%B3n+Secundaria+Ies+Rey+Fernando+VI!5e0!3m2!1ses!2ses!4v1465307669605"
		width="800" height="500" frameborder="0" style="border: 0"
		allowfullscreen></iframe>
	<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
	<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
</div>


