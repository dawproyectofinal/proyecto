
<?php if (isset($_SESSION['invitado'])) :?>
<div id="contenedorPrincipal">
	<!-- carousel -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img id="carusel" class="first-slide"
					src="<?= base_url()?>/assets/images/carousel1.jpg"
					alt="Primera Imagen">
			</div>
			<div class="item">
				<img id="carusel" class="second-slide"
					src="<?= base_url()?>/assets/images/carousel2.jpg"
					alt="Segunda imagen">
			</div>
			<div class="item">
				<img id="carusel" class="third-slide"
					src="<?= base_url()?>/assets/images/carousel3.jpg"
					alt="Tercera imagen">
			</div>
			<div class="item">
				<img id="carusel" class="third-slide"
					src="<?= base_url()?>/assets/images/carousel4.jpg"
					alt="Cuarta imagen">
			</div>
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#myCarousel"
			role="button" data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<!-- /.carousel -->
	<div id="portadaMenu">

		<div class="miniatura1"></div>
		<div class="miniatura2"></div>
		<a href="<?=base_url()?>invitado/realizarTest"><div class="miniatura3"></div></a>
		<div class="textoMiniatura1">
			<b>TODOS LOS PERMISOS</b>
		</div>
		<div class="textoMiniatura2">
			<b>MATRICULATE YA!!</b>
		</div>
		<div class="texto3">
			<b>CURSOS INTENSIVOS</b>
		</div>
	</div>
 <br><br><br><br><br><br> <br><br><br><br><br><br> <br><br><br><br><br><br> <br><br><br><br><br><br>
</div>
<?php else:?>
<h1>
	NO TIENES PERMISO <a href="<?= base_url()?>login/index">Volver</a>
</h1>
<?php endif;?>

