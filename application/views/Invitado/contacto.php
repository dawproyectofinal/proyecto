
<div class="textoQuienesSomos">
	<h2>CONTACTA CON NOSOTROS</h2>
	<hr>
	Puedes contactar con nosotros a través de nuestros teléfonos o bien
	rellenando el siguiente formulario (los campos marcados con * son
	necesarios): <br> <br>
	<p style="color: yellow"">
		<u>San Fernando de Henares</u>
	</p>
	<li style="margin-top: 12px;"><strong>Avd. Irún s/n (Oficina Central)</strong>
		Tlf. 91 673 02 41</li>
	<li style="margin-top: 12px;"><strong>Juan de la Cierva, 3 </strong>
		Tlf. 91 676 10 21</li>
	<li style="margin-top: 12px;"><strong>Avd. España, 15 </strong> Tlf. 91
		672 08 41</li>
	<li style="margin-top: 12px;"><strong>Miguel Delibes, 1 </strong> Tlf.
		91 675 14 50</li>
	<li style="margin-top: 12px;"><strong>Toledo, 42</strong> Tlf. 91 676
		40 14</li>
</div>
<br>
<br>
<br>

<div class="formularioContacto">
	<!--  <form class="form-horizontal" action="<?= base_url()?>correo" method="post">-->

	<form class="form-horizontal" 
		method="post">
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Nombre y
				apellidos *</label>
			<div class="col-md-4">
				<input id="textinput" name="nombre" type="text"
					placeholder="Nombre y apellidos" class="form-control input-md">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Teléfono</label>
			<div class="col-md-4">
				<input id="textinput" name="telefono" type="text"
					placeholder="Teléfono" class="form-control input-md">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Email *</label>
			<div class="col-md-4">
				<input id="textinput" name="email" type="text" placeholder="Email"
					class="form-control input-md">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textinput">Asunto</label>
			<div class="col-md-4">
				<input id="textinput" name="asunto" type="text" placeholder="Asunto"
					class="form-control input-md">

			</div>
		</div>

		<!-- Textarea -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="textarea">¿En qué podemos
				ayudarte? *</label>
			<div class="col-md-4">
				<textarea class="form-control" id="textarea" name="cuerpo"
					placeholder="¿En qué podemos ayudarte? ">
</textarea>
			</div>
		</div>

		<!-- Button (Double) -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="button1id"></label>
			<div class="col-md-8">
				<button id="button1id" name="button1id" class="btn btn-success">Enviar</button>

			</div>
		</div>

	</form>


	<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
	<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>

</div>