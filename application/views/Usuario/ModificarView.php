<?php if (isset($_SESSION['usuario'])) :?>
<fieldset>
	<legend>MODIFICACIÓN DE USUARIOS</legend>

	<!-- Form Name -->
	<form class="form-horizontal" name="formulario"
		action="<?= base_url()?>Usuario/modificar" method="POST">

<?php foreach($usuario as $x): ?>
<div class="form-group">
			<!-- Text input-->
			<label class="col-md-4 control-label" for="nombre">Nombre</label>
			<div class="col-md-5">
				<input type="hidden" id="id" name="id" value="<?= $x->id ?>" /> <input
					type="text" class="form-control input-md" id="nombre" name="nombre"
					value="<?= $x->nombre ?>" onchange="validaMod()" />
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="apellidos">Apellidos</label>
			<div class="col-md-5">
				<input type="text" class="form-control input-md" id="apellidos"
					name="apellidos" value="<?= $x->apellidos ?>"
					onchange="validaMod()" />
			</div>
		</div>


		<!-- Text input-->
		<div class="form-group">
			<label for="fnac" class="col-md-4 control-label">Fecha de Nacimiento</label>
			<div class="col-md-5">
				<input type="text" id="fnac" name="fnac"
					class="form-control input-md" value="<?= $x->fnac ?>"
					onchange="validaMod()" />
			</div>
		</div>


		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="email">Correo Electrónico</label>
			<div class="col-md-5">
				<input class="form-control input-md" id="email" name="email"
					value="<?= $x->email ?>" onchange="validaMod()" />
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="user">Usuario</label>
			<div class="col-md-5">
				<input type="text" id="user" name="user"
					class="form-control input-md" value="<?= $x->usuario ?>"
					onchange="validaMod('<?= $usuarios ?>')">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="password">Contraseña</label>
			<div class="col-md-5">
				<input type="text" id="password" name="password"
					placeholder="Contraseña" class="form-control input-md"
					autocomplete="off" onchange="validaMod()" />
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="email">Confirmar-Contraseña</label>
			<div class="col-md-5">
				<input type="text" id="password2" name="password2"
					placeholder="Contraseña" class="form-control input-md"
					autocomplete="off" onchange="validaMod()">
			</div>
		</div>

		<!-- Multiple Radios -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="tipo">Tipo de usuario</label>
			<div class="col-md-4">
	    <?php if($x->perfil == 0): ?>
	    <div class="radio">
					<label for="alumno"> <input type="radio" id="alumno" name="perfil"
						value="0" checked="checked" /> Alumno
					</label>
				</div>

				<div class="radio">
					<label for="profesor"> <input type="radio" id="profesor"
						name="perfil" value="1" /> Profesor
					</label>
				</div>
			</div>  
	     
	    <?php else: ?>
	    <div class="col-md-4">
				<div class="radio">
					<label for="alumno"> <input type="radio" id="alumno" name="perfil"
						value="0" /> Alumno
					</label>
				</div>

				<div class="radio">
					<label for="profesor"> <input type="radio" id="profesor"
						name="perfil" value="1" checked="checked" /> Profesor
					</label>
				</div>
			</div>
	     <?php endif; ?>
</div>
<?php endforeach; ?>

<!-- Button (Double) -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="mod"></label>
			<div class="col-md-8" id="botonesEnviarCancelar">
				<button type="button" id="mod" name="mod" class="btn btn-success"
					onclick="modificar(<?= $x->id?>)" disabled="disabled">Modificar</button>
				<button type="button" id="cancelar" name="cancelar"
					class="btn btn-danger" onclick="botonCancelar()">Cancelar</button>
			</div>
		</div>

	</form>
</fieldset>

<?php endif?>