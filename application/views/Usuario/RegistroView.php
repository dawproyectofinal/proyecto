

<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
if (isset ( $_SESSION ['usuario'] )) :
	?>

<script type="text/javascript"
	src="<?= base_url()?>assets/js/registroview.js"></script>
<script type="text/javascript"
	src="<?= base_url()?>assets/js/modificarview.js"></script>
<!-- <script src="<?php echo base_url().'assets/'?>js/registroview.js" type="text/javascript"></script> -->
<div id="contenedorPrincipal">
<br><br><br>
	<div id="resultado">
		<!-- Form Name -->
		<fieldset>
		
			<legend>ALTA DE USUARIOS</legend>

			<form class="form-horizontal" name="formulario"
				action="<?= base_url()?>Usuario/crear" method="POST">
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="nombre">Nombre</label>
					<div class="col-md-5">
						<input id="nombre" name="nombre" type="text" placeholder="Nombre"
							class="form-control input-md" autocomplete="off"
							onchange="validar()">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="apellidos">Apellidos</label>
					<div class="col-md-5">
						<input id="apellidos" name="apellidos" type="text"
							placeholder="Apellidos" class="form-control input-md"
							autocomplete="off" onchange="validar()">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="fnac">Fecha de
						nacimiento</label>
					<div class="col-md-5">
						<input id="fnac" name="fnac" type="text"
							placeholder="dd/mm/yyyy" class="form-control input-md"
							autocomplete="off" onchange="validar()">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Correo
						electrónico</label>
					<div class="col-md-5">
						<input id="email" name="email" type="text"
							placeholder="Correo electrónico" class="form-control input-md"
							autocomplete="off" onchange="validar()">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Usuario</label>
					<div class="col-md-5">
						<input id="user" name="user" type="text" placeholder="Usuario"
							class="form-control input-md" autocomplete="off"
							onchange="validar('<?= $usuarios ?>')">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Contraseña <br>(debe tener Mayúsculas, minúsculas, nº y 8 caracteres como mínimo)</label>
					<div class="col-md-5">
						<input id="password" name="password" type="text"
							placeholder="Contraseña" class="form-control input-md"
							autocomplete="off" onchange="validar()">

					</div>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Confirmar-Contraseña</label>
					<div class="col-md-5">
						<input id="password2" name="password2" type="text"
							placeholder="Contraseña" class="form-control input-md"
							autocomplete="off" onchange="validar()">

					</div>
				</div>

				<!-- Multiple Radios -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="perfil">Tipo de usuario</label>
					<div class="col-md-4">
						<div class="radio">
							<label for="alumno"> <input type="radio" name="perfil"
								id="alumno" value="0" onclick="validar()" disabled="disabled">
								Alumno
							</label>
						</div>
						<div class="radio">
							<label for="profesor"> <input type="radio" name="perfil"
								id="profesor" value="1" onclick="validar()" disabled="disabled">
								Profesor
							</label>
						</div>
					</div>
				</div>

				<!-- Button (Double) -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="alta"></label>
					<div class="col-md-8" id="botonesEnviarCancelar">
						<button id="alta" name="alta" class="btn btn-success"
							onclick="enviar()" disabled="disabled">Enviar</button>
						<button type="button" id="cancelar" name="cancelar"
							class="btn btn-danger" onclick="<?base_url()?>usuario/index">Cancelar</button>
					</div>
				</div>

			</form>
		</fieldset>
	</div>
	<br><br><br>
</div>
<?php endif?>