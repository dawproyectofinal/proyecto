<?php if (isset($_SESSION['usuario'])) :?>
<div id="textoPortadaUsuario">
	<b>PANEL DE ADMINISTRADOR</b>
</div>
<div id="portadaUsuario">
	<a href="<?=base_url()?>usuario/registro"><div class="crear" data-toggle="tooltip"
							data-placement="top" title="Registro de usuarios"></div></a>
	<a href="<?=base_url()?>usuario/listar"><div class="listar" data-toggle="tooltip"
							data-placement="top" title="Listar usuarios"></div></a>
	<a href="<?=base_url()?>cuestionario/index"><div class="crearP" data-toggle="tooltip"
							data-placement="top" title="Crear Preguntas"></div></a>
	<a href="<?=base_url()?>cuestionario/listarPreguntas"><div
			class="listarP" data-toggle="tooltip"
							data-placement="top" title="Listar preguntas"></div></a>


</div>
<?php else:?>
<h1>
	NO TIENES PERMISO <a href="<?= base_url()?>login/index">Volver</a>
</h1>
<?php endif;?>


<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>