
<header>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Usuario/registro"><span
				class="glyphicon glyphicon-user"></span> Crear Usuarios</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Usuario/listar"> <span
				class="glyphicon glyphicon-th-list"></span> Listar Usuarios
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand" href="<?= base_url()?>Cuestionario/index"> <span
				class="glyphicon glyphicon-tag"></span> Crear Preguntas
			</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>Cuestionario/listarPreguntas"> <span
				class="glyphicon glyphicon-tasks"></span> Listar Preguntas
			</a>
		</div>

		<div id="usuario"><?= isset($_SESSION['usuario'])?'Hola '.$_SESSION['usuario']:'Estas como INVITADO' ?></div>
		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>

	</nav>
	<!--/.navbar-collapse -->

</header>
<div id="contenedorPrincipal">