<?php if (isset($_SESSION['usuario'])) :?>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript"
	src="<?= base_url().'assets/'?>js/usuariosview.js"></script>
<script type="text/javascript"
	src="<?= base_url()?>assets/js/registroview.js"></script>
<script type="text/javascript"
	src="<?= base_url()?>assets/js/modificarview.js"></script>

<div id="contenedorPrincipal">
<br><br><br>
	<div id="resultado">

		<fieldset>
			<legend>LISTA DE USUARIOS</legend>

			<div class="form-group">
				<!--<label class="col-md-2 control-label" for="rol" id="labelFor"></label>-->
				<div class="col-md-2">
					<select class="form-control" name="filtro" id="filtro"
						onchange="filtrarPerfil(this.value)">
						<option value="titulo">Filtrar por..</option>
						<option value="0">Alumno</option>
						<option value="1">Profesor</option>
						<option value="all">TODOS</option>
					</select><br />
				</div>
				<br />
			</div>

			<table class="table table-hover">
				<tr id="encabezado">
					<th>NOMBRE</th>
					<th>APELLIDOS</th>
					<th>F. NACIMIENTO</th>
					<th>CORREO ELECTRÓNICO</th>
					<th>PERFIL</th>
				</tr>
	<?php foreach($usuarios as $user): ?>
	<tr>
					<td><?= $user->nombre ?> </td>
					<td><?= $user->apellidos ?> </td>
					<td><?= $user->fnac ?> </td>
					<td><?= $user->email ?> </td>
					<td><?php if($user->perfil == 0){echo 'ALUMNO';}else{echo 'PROFESOR';} ?></td>
					<td>
						<!-- onclick="<?= base_url().'Usuario/'?>modificar2" -->
						<button class="btn btn-primary" data-toggle="tooltip"
							data-placement="top" title="Editar"
							onclick="accion(<?= $user->id?>,'<?= $user->nombre?>','<?= $user->apellidos?>','M')">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						</button>
						<button class="btn btn-danger" data-toggle="tooltip"
							data-placement="top" title="Borrar"
							onclick="accion(<?= $user->id?>,'<?= $user->nombre?>','<?= $user->apellidos?>','B')">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</button>
					</td>
				</tr>
	<?php endforeach; ?>
</table>

		</fieldset>

	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<?php endif?>