
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container" id="headerCentral">
			<div class="navbar-header">
				<div class="textoLogotipo">
					AUTOESCUELA <img class="logotipo"
						src="<?= base_url()?>/assets/images/olmos_logo.png">
				</div>
			</div>
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar"></button>
			<a class="navbar-brand"
				href="<?= base_url()?>assets/calendario/index.php"><span
				class="glyphicon glyphicon-calendar"></span> Ver/editar Calendario</a>
		</div>

		<div id="usuario"><?= isset($_SESSION['profesor'])?'Hola '.$_SESSION['profesor']:'Estas como INVITADO' ?></div>
		<div class="navbar-header" id="cerrar">
			<a href="<?= base_url()?>login/cerrar">Cerrar Sesión</a>
		</div>
	</nav>
	<!--/.navbar-collapse -->


</header>
<div id="contenedorPrincipal">

