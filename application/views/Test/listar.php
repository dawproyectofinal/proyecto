<?php if (isset($_SESSION['usuario'])) :?>

<?php
	$cont = 0;
	?>

<div id="contenedorPrincipal">
<br><br><br>
	<div id="resultado">

		<h2>LISTA DE PREGUNTAS</h2>
		<table class="table table-hover">
			<tr id="encabezado">
				<th>Nº PREGUNTA</th>
				<th>ENUNCIADO</th>
			</tr>
				<?php foreach($preguntas as $preguntas): ?>  

		<tr>
				<td><?= $cont+1?> </td>
				<td><?= $preguntas->enunciado?></td>
				<td><button class="btn btn-primary" data-toggle="tooltip"
						data-placement="top" title="Editar"
						onclick="window.location.href='modificarPregunta?num=<?= $cont?>'">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</button>
					<button class="btn btn-danger" data-toggle="tooltip"
						data-placement="top" title="Borrar"
						onclick="window.location.href='borrarPregunta?num=<?= $cont?>'">
						<i class="fa fa-trash" aria-hidden="true"></i>
					</button></td>
			</tr>
	<?php $cont ++; ?>
	<?php endforeach; ?>

	</table>
 <br><br><br><br><br><br> <br><br><br><br><br><br>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<?php endif;?>