
<?php if (isset($_SESSION['usuario'])) :?>

<div id="contenedorPrincipal">
<br><br><br>
	<div id="resultado">
		<fieldset>
			<legend>CREACIÓN DE PREGUNTAS</legend>

			<br>
			<form class="form-horizontal"
				action="<?= base_url()?>Cuestionario/crearPregunta" method="Post">

				<!-- Textarea -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="textarea">Enunciado de
						la pregunta </label>
					<div class="col-md-7">
						<textarea class="form-control" name="pregunta"
							placeholder="Introduce el enunciado de la pregunta..."></textarea>
					</div>
				</div>

				<br>
				<!-- Multiple Radios -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="radios">Introduce las
						respuestas y selecciona la correcta</label>
					<div class="col-md-4" id="radios">
						<div class="radio">
							<label><input type="radio" name="correcta" value="0"
								checked="checked"> <input type="text" id="" size="100"
								name="res1" placeholder="Respuesta 1"
								class="form-control input-md"> </label>
						</div>
						<br>
						<div class="radio">
							<label><input type="radio" name="correcta" value="1"> <input
								type="text" id="" size="100" name="res2"
								placeholder="Respuesta 2" class="form-control input-md"> </label>
						</div>
						<br>
						<div class="radio">
							<label><input type="radio" name="correcta" value="2"> <input
								type="text" id="" size="100" name="res3"
								placeholder="Respuesta 3" class="form-control input-md"> </label>
						</div>

					</div>
				</div>

				<!-- Enviar/Cancelar -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="alta"></label>
					<div class="col-md-8" id="botonesEnviarCancelar">
						<button type="submit" id="alta" name="alta"
							class="btn btn-success" value="Enviar">Enviar</button>
						<button id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</button>
					</div>
				</div>
			</form>
	<?php
	if (isset ( $info )) {
		
		if ($info == "Todo correcto") {
			echo "<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>$info</div>";
		} else {
			echo "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>$info</div>";
		}
	}
	
	?>
</fieldset>
	</div>
</div>

<?php endif?>
