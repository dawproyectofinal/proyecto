
<?php if (isset($_SESSION['usuario'])) :?>
<?php

	$cor1 = "";
	$cor2 = "";
	$cor3 = "";
	if (( string ) $pregunta->respuesta [0] ['correcta'] == "si") {
		$cor1 = "checked";
	}
	if (( string ) $pregunta->respuesta [1] ['correcta'] == "si") {
		$cor2 = "checked";
	}
	if (( string ) $pregunta->respuesta [2] ['correcta'] == "si") {
		$cor3 = "checked";
	}
	?>
<div id="contenedorPrincipal">
	<div id="resultado">
		<h3>MODIFICACIÓN DE PREGUNTAS</h3>
		<hr>
		<br>
		<form class="form-horizontal"
			action="<?= base_url()?>Cuestionario/modificarPost" method="post">
			<div class="form-group">
				<label class="col-md-4 control-label" for="textarea">Enunciado de la
					pregunta </label>
				<div class="col-md-7">
					<textarea class="form-control" name="pregunta"><?=$pregunta->enunciado?></textarea>
				</div>
			</div>
			<br>
			<!-- Multiple Radios -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="radios">Introduce las
					respuestas y selecciona la correcta</label>
				<div class="col-md-4" id="radios">
					<div class="radio">
						<label><input type="radio" name="correcta" value="0" <?= $cor1?>>

							<input id="" name="res1" size="100" class="form-control input-md"
							value="<?=$pregunta->respuesta[0]?>"> </label>
					</div>
					<br>

					<div class="radio">
						<label><input type="radio" name="correcta" value="1" <?= $cor2?>>

							<input id="" name="res2" size="100" class="form-control input-md"
							value="<?=$pregunta->respuesta[1]?>"> </label>

					</div>

					<br>
					<div class="radio">
						<label><input type="radio" name="correcta" value="2" <?= $cor3?>>

							<input id="" name="res3" size="100" class="form-control input-md"
							value="<?=$pregunta->respuesta[2]?>"> </label>

					</div>

				</div>
			</div>

			<!-- Enviar/Cancelar -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="alta"></label>
				<div class="col-md-8" id="botonesEnviarCancelar">
					<button type="submit" id="alta" name="alta" class="btn btn-success"
						value="Enviar">Modificar</button>
					<button id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</button>
				</div>
			</div>
			<input type="hidden" name="id" value="<?= $pregunta['id']?>">
		</form>

	</div>
</div>


<?php endif;?>