	
	var base_url = "/proyecto";
	
	//Recoge los datos introducidos en ModificarView y hace las modificaciones.			
	function modificar(id){
		
		var nombre = document.getElementById('nombre').value;
		var apellidos = document.getElementById('apellidos').value;
		var fnac = document.getElementById('fnac').value;
		var correo = document.getElementById('email').value;
		var alumno = document.getElementById('alumno').checked;
		var profesor = document.getElementById('profesor').checked;
		var perfil = 0;
		
		if(profesor){
			perfil = 1;
		}
		
		var resultado = confirm('Modificar el usuario '+nombre+' '+apellidos+'?');
		
		if(resultado){
			
			if(resultado && validaMod()){
				alert('Usuario '+nombre+' '+apellidos+' modificado correctamente.');
				
				var xmlhttp=new XMLHttpRequest();
				xmlhttp.open('POST',base_url+'/Usuario/modificar',true);
				//xmlhttp.setRequestHeader('X-Requested-With','XMLHttpRequest');
				xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	            xmlhttp.send("id="+id+"&nombre="+nombre+"&apellidos="+apellidos+"&fnac="+fnac+"&email="+correo+"&perfil="+perfil);
				//xmlhttp.send();
	            
					xmlhttp.onreadystatechange=function(){
						if(xmlhttp.readyState==4 && xmlhttp.status==200){
							document.getElementById("resultado").innerHTML = xmlhttp.responseText;
						}
					}	
			}
			else{
				alert('Datos introducidos no validos! Vuelva a intentarlo.');
			}
			
		}
		else{
			alert('Modificacion de  usuario cancelada.');
			window.location=base_url+"/Usuario/listar";
		}	
	}
	
	//Redirige a listar el boton CANCELAR==========================================================
	function botonCancelar(){
			window.location=base_url+"/Usuario/listar";
	}
	
	//Comprueba que las modificaciones en ModificarView sean validas.===============================================
	function validaMod(){
		flag = false;
		
		nombre = validaNombre();
		apellidos = validaApellidos();
		fecha = validaFnac();
		correo = validaEmail();

		if(nombre && apellidos && fecha && correo){	
			flag = true;
		}
	return flag;
	}
	