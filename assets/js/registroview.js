//========================================================VALIDA NOMBRE
function validaNombre() {
	control = /^([a-z ñáéíóú]{2,35})$/i;
	flag = false;

	if (control.test(formulario.nombre.value)) {
		formulario.nombre.style.color = "green";
		flag = true;
	} else {
		formulario.nombre.style.color = "gray";
		flag = false;
	}
	return flag;
}

// =====================================================VALIDA APELLIDOS
function validaApellidos() {
	control = /^([a-z ñáéíóú]{2,35})$/i;
	flag = false;

	if (control.test(formulario.apellidos.value)) {
		formulario.apellidos.style.color = "green";
		flag = true;
	} else {
		formulario.apellidos.style.color = "gray";
		flag = false;
	}
	return flag;
}

// ======================================================VALIDA FNAC
function validaFnac() {
	control = /^([0][1-9]||[1][0-9]||[2][0-9]||[3][0-1])\/([0][1-9]||[1][0-2])\/([1][9][1-9][0-9]||[2][0][0-1][0-6])$/;
	flag = false;

	if (control.test(formulario.fnac.value)) {
		formulario.fnac.style.color = "green";
		flag = true;
	} else {
		formulario.fnac.style.color = "gray";
		flag = false;
	}
	return flag;
}

// =======================================================VALIDA EMAIL
function validaEmail() {
	control = /^[0-9a-z._]{3,20}\@[a-z._]{3,15}\.[a-z]{2,3}$/;
	flag = false;

	if (control.test(formulario.email.value)) {
		formulario.email.style.color = "green";
		flag = true;
	} else {
		formulario.email.style.color = "gray";
		flag = false;
	}
	return flag;
}

// ========================================================VALIDA USUARIO
function validaUsuario(usuarios) {

	if (usuarios == undefined) {
		usuarios = '';
	}

	var user = document.getElementById('user').value;

	if (user != '') {
		var n = usuarios.search(user);

		if (n == -1) {
			formulario.user.style.color = "green";
			flag = true;
		} else {
			formulario.user.style.color = "gray";
			flag = false;
			alert('Usuario introducido ya existe!');
		}
	} else {
		flag = false;
	}
	return flag;
}

// ========================================================VALIDA PASSWORD
function validaPassword() {

	var control = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{8,}$/;
	flag = false;

	if (control.test(formulario.password.value)) {
		formulario.password.style.color = "green";

		if (formulario.password.value == formulario.password2.value) {
			formulario.password2.style.color = "green";
			document.getElementById('alumno').disabled = false;
			document.getElementById('profesor').disabled = false;
			flag = true;
		} else if (formulario.password2.value != '') {
			alert('La contraseña debe coincidir!');
		}
	} else {
		formulario.password.style.color = "gray";
		flag = false;
		// alert('Contraseña no válida!');
	}
	return flag;
}

// ========================================================VALIDA PERFIL
function validaPerfil() {
	flag = false;

	if (formulario.perfil.value == '0' || formulario.perfil.value == '1') {
		flag = true;
	} else {
		flag = false;
	}
	return flag;
}

// =========================================================VALIDAR
function validar(usuarios) {

	nombre = validaNombre();
	// alert(nombre);
	apellidos = validaApellidos();
	// alert(apellidos);
	fecha = validaFnac();
	// alert(fecha);
	correo = validaEmail();
	// alert(correo);
	usuario = validaUsuario(usuarios);
	// alert(usuario);
	password = validaPassword();
	// alert(password);
	perfil = validaPerfil();
	// alert(perfil);

	if (nombre && apellidos && fecha && correo && usuario && password && perfil) {
		formulario.alta.disabled = false;
		formulario.nombre.readOnly = true;
		formulario.apellidos.readOnly = true;
		formulario.fnac.readOnly = true;
		formulario.email.readOnly = true;
		formulario.user.readOnly = true;
		formulario.password.readOnly = true;
		formulario.password2.readOnly = true;
		formulario.perfil.readOnly = true;
	} else {
		formulario.alta.disabled = true;
	}
}

// ===========================================================ENVIAR
function enviar() {
	alert('Usuario registrado correctamente.');
}
