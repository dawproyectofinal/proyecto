var base_url = "/proyecto";

// Recoge los datos introducidos en ModificarView y hace las modificaciones.
function modificar(id) {

	var nombre = document.getElementById('nombre').value;
	var apellidos = document.getElementById('apellidos').value;
	var fnac = document.getElementById('fnac').value;
	var correo = document.getElementById('email').value;
	var usuario = document.getElementById('user').value;
	var password = document.getElementById('password').value;
	var alumno = document.getElementById('alumno').checked;
	var profesor = document.getElementById('profesor').checked;
	var perfil = 0;

	if (profesor) {
		perfil = 1;
	}

	var resultado = confirm('Modificar el usuario ' + nombre + ' ' + apellidos
			+ '?');

	if (resultado) {

		if (resultado && validaMod()) {
			alert('Usuario ' + nombre + ' ' + apellidos
					+ ' modificado correctamente.');

			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open('POST', base_url + '/Usuario/modificar', true);
			// xmlhttp.setRequestHeader('X-Requested-With','XMLHttpRequest');
			xmlhttp.setRequestHeader("Content-Type",
					"application/x-www-form-urlencoded");
			xmlhttp.send("id=" + id + "&nombre=" + nombre + "&apellidos="
					+ apellidos + "&fnac=" + fnac + "&email=" + correo
					+ "&user=" + usuario + "&password=" + password + "&perfil="
					+ perfil);
			// xmlhttp.send();

			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					document.getElementById("resultado").innerHTML = xmlhttp.responseText;
				}
			}
		} else {
			alert('Datos introducidos no validos! Vuelva a intentarlo.');
		}

	} else {
		alert('Modificacion de  usuario cancelada.');
		window.location = base_url + "/Usuario/listar";
	}
}

// Redirige a listar el boton
// CANCELAR==========================================================
function botonCancelar() {
	window.location = base_url + "/Usuario/listar";
}

// Comprueba que las modificaciones en ModificarView sean
// validas.===============================================
function validaMod(usuarios) {
	flag = false;

	nombre = validaNombre();
	// alert(nombre);

	apellidos = validaApellidos();
	// alert(apellidos);

	fecha = validaFnac();
	// alert(fecha);

	correo = validaEmail();
	// alert(correo);

	usuario = validaUsuario(usuarios);
	// alert(usuario);

	password = validaPassword();
	// alert(password);

	perfil = validaPerfil();
	// alert(perfil);

	if (nombre && apellidos && fecha && correo && usuario && password && perfil) {
		formulario.mod.disabled = false;
		formulario.nombre.readOnly = true;
		formulario.apellidos.readOnly = true;
		formulario.fnac.readOnly = true;
		formulario.email.readOnly = true;
		formulario.user.readOnly = true;
		formulario.password.readOnly = true;
		formulario.password2.readOnly = true;
		formulario.perfil.readOnly = true;
		flag = true;
	} else {
		formulario.mod.disabled = true;
	}
	return flag;
}
