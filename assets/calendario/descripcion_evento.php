<?php
// incluimos nuestro archivo config
include 'config.php';

// Incluimos nuestro archivo de funciones
include 'funciones.php';

// Obtenemos el id del evento
$id = evaluar ( $_GET ['id'] );

// y lo buscamos en la base de dato
$bd = $conexion->query ( "SELECT * FROM eventos WHERE id=$id" );

// Obtenemos los datos
$row = $bd->fetch_assoc ();

// titulo
$titulo = $row ['title'];

// cuerpo
$evento = $row ['body'];

// Fecha inicio
$inicio = $row ['inicio_normal'];

// Fecha Termino
$final = $row ['final_normal'];

// Eliminar evento
if (isset ( $_POST ['eliminar_evento'] )) {
	$id = evaluar ( $_GET ['id'] );
	$sql = "DELETE FROM eventos WHERE id = $id";
	if ($conexion->query ( $sql )) {
		echo "<div class=\"alert alert-success alert-dismissable\"> 			
				EVENTO ELIMINADO</div>";
				
	} else {
		echo "El evento no se pudo eliminar";
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="<?=$base_url?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$base_url?>js/es-ES.js"></script>
<script src="<?=$base_url?>js/jquery.min.js"></script>
<script src="<?=$base_url?>js/moment.js"></script>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

<title><?=$titulo?></title>
</head>
<body>
	<h2><?=$titulo?></h2>
	<hr>
	<h4><?=$evento?></h4>
</body>
<br>
<br>
<br>
<?php if(!isset($_SESSION)) { session_start(); }?>
<?php if (isset($_SESSION['profesor'])) :?>
<form action="" method="post">
	<button type="submit" class="btn btn-danger" name="eliminar_evento"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar
		evento</button>
</form>
<?php endif;?>
</html>
