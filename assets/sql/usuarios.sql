-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2016 a las 22:31:28
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(5) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `fnac` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `perfil` int(1) NOT NULL,
  `usuario` varchar(10) NOT NULL,
  `password` blob NOT NULL,
  `keycrypt` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `fnac`, `email`, `perfil`, `usuario`, `password`, `keycrypt`) VALUES
(2, 'Sergio', 'Olmos Paredes', '1992-08-28', 'olmos_555@hotmail.com', 1, 'solmos', 0x7365726769, ''),
(8, 'Muhamed', 'Ali', '2010-04-05', 'ali.baba401@gmail.com', 0, 'alibaba40', 0x414c49626162613832, ''),
(10, 'Anibal', 'Leckter', '1992-02-04', 'anibal.leckter@gmail.com', 1, 'anibal98', 0x416e6962616c363636, ''),
(11, 'Bruno', 'Barreto', '1982-02-04', 'bruno.barreto@gmail.com', 0, 'bbarreto', 0x313233343536, ''),
(13, 'Jose', 'Garcia', '1998-08-04', 'jose.garcia@gmail.com', 1, 'joselito', 0x62616e62616e, ''),
(18, 'Julio', 'Ricardo', '1990-06-04', 'julio.ricardo@hotmail.com', 0, 'juliorick', 0x243524726f756e64733d3530303024757365736f6d6573696c6c797374726924675a74462f6a655a5171754274535875356e52566c516955745a4d4d4e477859514d6f375732554a746c31, 0x2455285532384a3239385f3a2d7c6d6b6d6a6875697966214b4d496e786e733f646a494fc2bf3f293955574538593775726476),
(20, 'Profesor', 'Profesor', '1989-11-09', 'olmos_555@hotmail.com', 1, 'profesor', 0x75644d5637666658745850396479504c2f365248326f4c5a2f4a65305358412f7a596331475677397374673d, ''),
(22, 'Admin', 'Admin', '1989-11-09', 'olmos_555@hotmail.com', 2, 'admin', 0x62535231453038756e4d546e34306e4d2f714c704d387267436f54754945446139466233464d58417072383d, ''),
(23, 'Alumno', 'Alumno', '1989-11-09', 'olmos_555@hotmail.com', 0, 'alumno', 0x67502f39626978677355564741426c71505a774a7275396d6577426b704a4e7a527161464d586f70364c6f3d, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
